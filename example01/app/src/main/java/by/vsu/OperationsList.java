package by.vsu;

import java.util.HashMap;

public class OperationsList {
    private static final HashMap<Integer, Operation> operations = new HashMap<>();
    static {
        operations.put(R.id.addMenuItem, new OperationAdd());
        operations.put(R.id.subMenuItem, new OperationSub());
        operations.put(R.id.mulMenuItem, new OperationMul());
        operations.put(R.id.divMenuItem, new OperationDiv());
    }

    public static Operation getOperation(Integer id) {
        return operations.get(id);
    }
}
