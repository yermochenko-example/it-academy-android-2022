package by.vsu;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

public class ButtonNewOnClickListener implements View.OnClickListener {
    private final MainActivity mainActivity;

    public ButtonNewOnClickListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(mainActivity, AboutActivity.class);
        mainActivity.startActivity(intent);
    }
}
