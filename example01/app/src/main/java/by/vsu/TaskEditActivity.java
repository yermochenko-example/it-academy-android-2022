package by.vsu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class TaskEditActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_edit);

        EditText titleEditText = findViewById(R.id.activityTaskEditEditTextTitle);
        EditText descriptionEditText = findViewById(R.id.activityTaskEditEditTextDescription);
        EditText dateEditText = findViewById(R.id.activityTaskEditEditTextDate);
        EditText priorityEditText = findViewById(R.id.activityTaskEditEditTextPriority);
        CheckBox statusCheckBox = findViewById(R.id.activityTaskEditCheckBoxStatus);

        Button saveTaskButton = findViewById(R.id.activityTaskEditButtonSaveTask);
        saveTaskButton.setOnClickListener(view -> {
            try {
                Task task = new Task();
                String title = titleEditText.getText().toString();
                if(title.isEmpty()) {
                    throw new IllegalArgumentException("Заголовок задачи не должен быть пустым");
                }
                task.setTitle(title);
                String description = descriptionEditText.getText().toString();
                task.setDescription(description);
                String deadline = dateEditText.getText().toString();
                if(deadline.isEmpty()) {
                    throw new IllegalArgumentException("Срок выполнения задачи не должен быть пустым");
                }
                task.setDeadline(deadline);
                double priority = Double.parseDouble(priorityEditText.getText().toString());
                if(priority < 0 || priority > 1) {
                    throw new IllegalArgumentException("Приоритет задачи должен быть числом от 0 до 1");
                }
                task.setPriority(priority);
                task.setActive(statusCheckBox.isChecked());
                try(DatabaseHelper helper = new DatabaseHelper(this)) {
                    DatabaseTaskHandler handler = helper.createDatabaseTaskHandler();
                    handler.create(task);
                    startActivity(new Intent(this, TaskListActivity.class));
                } catch (SQLException e) {
                    Log.e("database", "can't load info", e);
                    Toast.makeText(this, "Данные не удалось сохранить", Toast.LENGTH_LONG).show();
                }
            } catch (IllegalArgumentException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
