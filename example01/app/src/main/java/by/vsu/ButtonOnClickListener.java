package by.vsu;

import android.view.View;
import android.widget.Toast;

public class ButtonOnClickListener implements View.OnClickListener {
    private final MainActivity mainActivity;
    private final Toast firstNumberErrorToast;
    private final Toast secondNumberErrorToast;

    public ButtonOnClickListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        firstNumberErrorToast = Toast.makeText(mainActivity, "Первое число не должно быть пустым", Toast.LENGTH_LONG);
        secondNumberErrorToast = Toast.makeText(mainActivity, "Второе число не должно быть пустым", Toast.LENGTH_LONG);
    }

    @Override
    public void onClick(View view) {
        String text = mainActivity.getButton().getText().toString();
        Operation operation = mainActivity.getOperation();
        String firstNumber = mainActivity.getEditFirstNumber().getText().toString();
        if(firstNumber.isEmpty()) {
            firstNumberErrorToast.show();
            return;
        }
        double x = Double.parseDouble(firstNumber);
        String secondNumber = mainActivity.getEditSecondNumber().getText().toString();
        if(secondNumber.isEmpty()) {
            secondNumberErrorToast.show();
            return;
        }
        double y = Double.parseDouble(secondNumber);
        double z = operation.calc(x, y);
        mainActivity.getResultTextView().setText("Результат операции равен " + z);
        Toast.makeText(mainActivity, "Произведено действие \"" + text + "\"", Toast.LENGTH_LONG).show();
    }
}
