package by.vsu;

public class OperationDiv extends Operation {
    @Override
    public double calc(double x, double y) {
        return x / y;
    }
}
