package by.vsu;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class MainActivity extends GeneralActivity {
    private TextView operationLabelTextView;
    private EditText editFirstNumber;
    private EditText editSecondNumber;
    private TextView resultTextView;
    private Button button;
    private Operation operation = OperationsList.getOperation(R.id.addMenuItem);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        operationLabelTextView = findViewById(R.id.operationLabelTextView);
        editFirstNumber = findViewById(R.id.editFirstNumber);
        editSecondNumber = findViewById(R.id.editSecondNumber);
        resultTextView = findViewById(R.id.resultTextView);
        button = findViewById(R.id.button);
        button.setOnClickListener(new ButtonOnClickListener(this));
        Button buttonNew = findViewById(R.id.buttonNew);
        buttonNew.setOnClickListener(new ButtonNewOnClickListener(this));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        operation = OperationsList.getOperation(item.getItemId());
        String operationName = item.getTitle().toString();
        Toast.makeText(this, "Выбрано действие \"" + operationName + "\"", Toast.LENGTH_LONG).show();
        operationLabelTextView.setText(operationName);
        button.setText(operationName);
        return super.onOptionsItemSelected(item);
    }

    public EditText getEditFirstNumber() {
        return editFirstNumber;
    }

    public EditText getEditSecondNumber() {
        return editSecondNumber;
    }

    public TextView getResultTextView() {
        return resultTextView;
    }

    public Button getButton() {
        return button;
    }

    public Operation getOperation() {
        return operation;
    }
}
