package by.vsu;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class TaskListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        ListView listView = findViewById(R.id.activityTaskListListViewTasks);
        try(DatabaseHelper helper = new DatabaseHelper(this)) {
            DatabaseTaskHandler handler = helper.createDatabaseTaskHandler();
            List<Task> tasks = handler.readAll();
            ArrayAdapter<Task> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tasks);
            listView.setAdapter(adapter);
        } catch (SQLException e) {
            Log.e("database", "can't load info", e);
        }

        Button button = findViewById(R.id.activityTaskListButtonAddTasks);
        button.setOnClickListener(view -> {
            Intent intent = new Intent(this, TaskEditActivity.class);
            startActivity(intent);
        });
    }
}
