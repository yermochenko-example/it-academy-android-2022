package by.vsu;

public class OperationAdd extends Operation {
    @Override
    public double calc(double x, double y) {
        return x + y;
    }
}
