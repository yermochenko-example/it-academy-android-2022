package by.vsu;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DatabaseTaskHandler {
    private final SQLiteDatabase db;

    public DatabaseTaskHandler(SQLiteDatabase db) {
        this.db = db;
    }

    @SuppressLint("Range")
    public List<Task> readAll() {
        List<Task> tasks = new ArrayList<>();
        try(Cursor cursor = db.rawQuery("SELECT id, title, description, deadline, priority, is_active FROM task", new String[0])) {
            if(cursor != null && cursor.moveToFirst()) {
                do {
                    Task task = new Task();
                    task.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    task.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                    task.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    task.setDeadline(cursor.getString(cursor.getColumnIndex("deadline")));
                    task.setPriority(cursor.getDouble(cursor.getColumnIndex("priority")));
                    task.setActive(cursor.getInt(cursor.getColumnIndex("is_active")) != 0);
                    tasks.add(task);
                } while (cursor.moveToNext());
            }
        }
        return tasks;
    }

    public void create(Task task) {
        db.execSQL(
            "INSERT INTO task(title, description, deadline, priority, is_active) VALUES (?, ?, ?, ?, ?)",
            new Object[] {
                task.getTitle(),
                task.getDescription(),
                task.getDeadline(),
                task.getPriority(),
                task.isActive() ? 1L : 0L
            }
        );
    }
}
