package by.vsu;

public abstract class Operation {
    public abstract double calc(double x, double y);
}
