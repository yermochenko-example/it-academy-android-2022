package by.vsu;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, "tasks_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE task (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "title TEXT," +
                "description TEXT," +
                "deadline TEXT," + // дата в формате YYYY-MM-DD
                "priority REAL," + // одно из чисел [0, 0.25, 0.5, 0.75, 1]
                "is_active INTEGER" + // логическое значение 0 (false) или 1 (true)
        ")");
        db.execSQL("INSERT INTO task(title, description, deadline, priority, is_active)" +
                "VALUES ('Земля', 'Стихия земли', '2023-08-15', 0.5, 1)");
        db.execSQL("INSERT INTO task(title, description, deadline, priority, is_active)" +
                "VALUES ('Огонь', 'Стихия огня', '2023-06-01', 0.75, 1)");
        db.execSQL("INSERT INTO task(title, description, deadline, priority, is_active)" +
                "VALUES ('Воздух', 'Стихия воздуха', '2023-12-25', 1, 1)");
        db.execSQL("INSERT INTO task(title, description, deadline, priority, is_active)" +
                "VALUES ('Вода', 'Стихия воды', '2023-04-01', 0, 0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public DatabaseTaskHandler createDatabaseTaskHandler() {
        return new DatabaseTaskHandler(getWritableDatabase());
    }
}
