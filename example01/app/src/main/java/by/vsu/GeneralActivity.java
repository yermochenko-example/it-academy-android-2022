package by.vsu;

import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;

public class GeneralActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
