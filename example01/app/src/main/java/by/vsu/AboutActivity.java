package by.vsu;

import android.os.Bundle;
import android.widget.Button;

public class AboutActivity extends GeneralActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Button buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new ButtonNextOnClickListener(this));
    }
}
