package by.vsu;

import android.content.Intent;
import android.view.View;

public class ButtonNextOnClickListener implements View.OnClickListener {
    private final AboutActivity aboutActivity;

    public ButtonNextOnClickListener(AboutActivity aboutActivity) {
        this.aboutActivity = aboutActivity;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(aboutActivity, AuthorActive.class);
        aboutActivity.startActivity(intent);
    }
}
