package by.vsu;

public class OperationMul extends Operation {
    @Override
    public double calc(double x, double y) {
        return x * y;
    }
}
