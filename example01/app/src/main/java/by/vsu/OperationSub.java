package by.vsu;

public class OperationSub extends Operation {
    @Override
    public double calc(double x, double y) {
        return x - y;
    }
}
